[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jmduarte1%2Frta-workshop/master)

The list of jupyter notebooks for RTA Workshop:

 - DataGenerator
 - KerasCNN1D
 - GWExample

Design sensitivity curve was created for Virgo Interferometer.

Required Python packages:

 - numpy
 - scipy
 - matplotlib
 - scikit-learn
 - tensorflow
 - keras
 - statsmodels
 - h5py